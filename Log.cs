﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityUtils;
using System.Collections.Generic;

namespace UnityUtils{
	public class MessageLog{
		private string _text;
		public enum MessageType{INFO, WARNING, ERROR}
		public string Text {
			get {
				return this._text;
			}
		}

		public MessageLog(string text, MessageType type){
			switch (type) {
			case MessageType.INFO:
				_text = "<color=#"+Log.InfoTextColor.ToHex()+">"+ text.ToString()+"</color>";
				break;
			case MessageType.WARNING:
				_text = "<color=#"+Log.WarningTextColor.ToHex()+"><i>"+ text.ToString()+"</i></color>";
				break;
			case MessageType.ERROR:
				_text = "<color=#"+Log.ErrorTextColor.ToHex () + "><b>" + text.ToString () + "</b></color>";
				break;
			default:
				break;
			}
		}
	}

	public static class Log{
		public enum LogLevel{INFO, WARNING, ERROR, NONE}
		public static LogLevel logLevel;
		private static Text _text;
		private static Color[] _colors = new Color[3]{Color.white, Color.yellow, Color.red};
		private static int _maxLogCount = 50;
		private static int _currentLogCount;
		private static List<MessageLog> messages;
		private static string Text {
			get {
				return _text.text;
			}
			set {
				_text.text += value + "\n";
			}
		}
		public static Color InfoTextColor {
			get {
				return _colors[0];
			}
			set{
				_colors[0] = value;
			}
		}
		public static Color WarningTextColor {
			get {
				return _colors[1];
			}
			set{
				_colors[1] = value;
			}
		}
		public static Color ErrorTextColor {
			get {
				return _colors[2];
			}
			set{
				_colors[2] = value;
			}
		}

		private static void print(object text, MessageLog.MessageType type){
			switch (logLevel) {
			case LogLevel.NONE:
				return;
			case LogLevel.ERROR:
				if (type != MessageLog.MessageType.ERROR) {
					return;
				}
				break;
			case LogLevel.WARNING:
				if (type != MessageLog.MessageType.ERROR && type != MessageLog.MessageType.WARNING) {
					return;
				}
				break;
			default:
				break;
			}
			if (messages == null) {
				messages = new List<MessageLog> ();
			}
			messages.Add (new MessageLog (text.ToString (), type));
			CreateOrFindDebugCanvas ();
			_currentLogCount = messages.Count;
			_text.text = "";
			if (_currentLogCount > _maxLogCount) {
				messages.RemoveRange (0, 1);
			}
			foreach(MessageLog message in messages){
				Text = message.Text;
			}
		}

		private static void printf(string format, object[] args, MessageLog.MessageType type){
			print (string.Format (format, args), type);
		}

		public static void Info(string format, params object[]args){
			printf (format, args, MessageLog.MessageType.INFO);
			Debug.LogFormat (format, args);
		}

		public static void Warning(string format, params object[]args){
			printf (format, args, MessageLog.MessageType.WARNING);
			Debug.LogWarningFormat (format, args);
		}

		public static void Error(string format, params object[]args){
			printf (format, args, MessageLog.MessageType.ERROR);
			Debug.LogErrorFormat (format, args);
		}

		public static void Info(object message){
			print (message, MessageLog.MessageType.INFO);
			Debug.Log (message);
		}

		public static void Warning(object message){
			print (message, MessageLog.MessageType.WARNING);
			Debug.LogWarning (message);
		}

		public static void Error(object message){
			print (message, MessageLog.MessageType.ERROR);
			Debug.LogError (message);
		}

		private static void CreateOrFindDebugCanvas(){
			GameObject canvas = GameObject.Find ("Debug Canvas");
			if (canvas == null) {
				canvas = new GameObject ("Debug Canvas", typeof(Canvas), typeof(CanvasScaler));
				Canvas c = canvas.GetComponent<Canvas> ();
				c.renderMode = RenderMode.ScreenSpaceOverlay;
				c.sortingOrder = 1000;
				GameObject t = new GameObject ("Text", typeof(Text), typeof(Shadow));
				t.transform.SetParent (canvas.transform);

				RectTransform rt = t.GetComponent<RectTransform> ();
				rt.anchorMin = new Vector2 (0, 0);
				rt.anchorMax = new Vector2 (1, 1);
				rt.pivot = new Vector2 (0.5f, 0.5f);
				rt.sizeDelta = Vector2.zero;
				rt.localPosition = Vector2.zero;

				_text = t.GetComponent<Text> ();
				_text.font = Font.CreateDynamicFontFromOSFont("Arial", 12);
				_text.color = Color.black;
				_text.resizeTextForBestFit = true;
				_text.alignment = TextAnchor.LowerLeft;
				_text.resizeTextMinSize = 10;
				_text.resizeTextMaxSize = 20;
				_text.verticalOverflow = VerticalWrapMode.Overflow;
			}
		}
	}
}