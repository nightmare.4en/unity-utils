﻿using System;
using UnityEngine;

namespace UnityUtils
{
	public static class ColorExtesionMethods{
		public static string ToHex(this Color color)
		{
			Color32 c = new Color32 ((byte)(color.r * 255), (byte)(color.g * 255),(byte)(color.b * 255), 255);
			string hex = c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2");
			return hex;
		}
	}
}

