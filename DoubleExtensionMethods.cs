﻿using System;

namespace UnityUtils
{
	public static class DoubleExtensionMethods
	{
		public static DateTime ToDateTime(this double d)
		{
			DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(d).ToLocalTime();
			return dtDateTime;
		}
	}
}

