﻿using System;
using UnityEngine;
namespace UnityUtils
{
	public static class GameObjectExtesionMethods{
		public static void Destroy(this UnityEngine.Object obj)
		{
			MonoBehaviour.Destroy (obj);
		}
	}
}